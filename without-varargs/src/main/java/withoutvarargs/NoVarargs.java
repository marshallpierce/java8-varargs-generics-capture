package withoutvarargs;

class NoVarargs {
    private void foo() {
        GenericContainerWithoutVarargs<?> container = new GenericContainerWithoutVarargs<>();
        container.add(new Object());
    }
}
