package withvarargs;

class GenericContainerWithVarargs<T> {

    void add(T obj) {

    }

    void add(T... objects) {

    }

}
