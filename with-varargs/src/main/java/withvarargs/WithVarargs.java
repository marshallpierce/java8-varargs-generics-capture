package withvarargs;

class WithVarargs {
    private void foo() {
        GenericContainerWithVarargs<?> container = new GenericContainerWithVarargs<>();
        container.add(new Object());
    }
}
