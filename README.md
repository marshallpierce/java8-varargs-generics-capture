

`GenericContainerWithVarargs<T>` has `add(T)` and `add(T...)`. This sample code (`WithVarargs`) that invokes `add(new Object())` compiles under Java 7, but doesn't under Java 8 M8 b127:
```
GenericContainerWithVarargs<?> container = new GenericContainerWithVarargs<>();
container.add(new Object());
```

 `GenericContainerWithoutVarargs` has only `add(T)`. This other sample code (`NoVarargs`) that also invokes `add(new Object())` doesn't compile under either version:
 ```
 GenericContainerWithoutVarargs<?> container = new GenericContainerWithoutVarargs<>();
 container.add(new Object());
 ```



To reproduce this, try the following under both JDK 7 and JDK 8. There are two separate modules so that compile failures in one don't affect the other.
```
./gradlew clean :with-varargs:build
./gradlew clean :without-varargs:build
```

Under JDK 7, `with-varargs` compiles (with an unchecked warning) but `without-varargs` does not:

```
/.../without-varargs/src/main/java/withoutvarargs/GenericVarargs.java:6: error: method add in class GenericContainerWithoutVarargs<T> cannot be applied to given types;
        container.add(new Object());
                 ^
  required: CAP#1
  found: Object
  reason: actual argument Object cannot be converted to CAP#1 by method invocation conversion
  where T is a type-variable:
    T extends Object declared in class GenericContainerWithoutVarargs
  where CAP#1 is a fresh type-variable:
    CAP#1 extends Object from capture of ?
1 error
```

Under JDK 8 M8 b127, neither builds.

`with-varargs`:

```
/.../with-varargs/src/main/java/withvarargs/GenericContainerWithVarargs.java:9: warning: [unchecked] Possible heap pollution from parameterized vararg type T
    void add(T... objects) {
                  ^
  where T is a type-variable:
    T extends Object declared in class GenericContainerWithVarargs
/.../with-varargs/src/main/java/withvarargs/GenericVarargs.java:6: error: no suitable method found for add(Object)
        container.add(new Object());
                 ^
    method GenericContainerWithVarargs.add(CAP#1) is not applicable
      (argument mismatch; Object cannot be converted to CAP#1)
    method GenericContainerWithVarargs.add(CAP#1...) is not applicable
      (varargs mismatch; Object cannot be converted to CAP#1)
  where CAP#1 is a fresh type-variable:
    CAP#1 extends Object from capture of ?
1 error
1 warning
```

`without-varargs`:
```
/.../without-varargs/src/main/java/withoutvarargs/GenericVarargs.java:6: error: method add in class GenericContainerWithoutVarargs<T> cannot be applied to given types;
        container.add(new Object());
                 ^
  required: CAP#1
  found: Object
  reason: argument mismatch; Object cannot be converted to CAP#1
  where T is a type-variable:
    T extends Object declared in class GenericContainerWithoutVarargs
  where CAP#1 is a fresh type-variable:
    CAP#1 extends Object from capture of ?
1 error
```

This is a simplified version of this code in Gradle's [DefaultModelRegistry](https://github.com/gradle/gradle/blob/75785398f19b634494316b0eb8f2427ab6adbc1a/subprojects/core/src/main/groovy/org/gradle/model/internal/DefaultModelRegistry.java#L210) that has an analogous failure:
```
private Inputs toInputs(Iterable<ModelPath> inputPaths) {
    ImmutableList.Builder<?> builder = ImmutableList.builder();
    for (ModelPath inputPath : inputPaths) {
        builder.add(getClosedModel(inputPath));
    }
    return new DefaultInputs(builder.build());
}
```

The Guava ImmutableList.Builder that it's using basically has the `with-varargs` case: both `add(E e)` and `add(E... es)` are provided, so it compiled under Java 7 and now doesn't.
